export default class Explosives {
	constructor(elementId, canvas, actions) {
		if (this.constructor === Explosives) {
			throw new TypeError(
				'Abstract class "Explosives" cannot be instantiated directly.'
			);
		}
		if (this.init === undefined) {
			throw new TypeError(
				'Classes extending the "Explosives" abstract class must implement required methods'
			);
		}
		this.elementId = elementId;
		this.canvas = canvas;
		this.actions = actions;
		this.element = null;
	}

	// init() - required method to implement

	render() {
		this.element = this.createSvgElement();
		this.canvas.append(this.element);
	}

	createSvgElement() {
		let svg = '<svg><use xlink:href="#' + this.elementId + '"></use></svg>';
		let el = document.createElement("div");
		el.classList.add("icon");
		el.innerHTML = svg;
		return el;
	}

	executeAction() {
		if (this.actions.length) {
			let action = this.actions.shift();
			this.changeState(action);
			this[action](() => {
				this.executeAction();
			});
		} else {
			console.warn("fail: no action left to execute");
		}
	}

	changeState(action) {
		let actionFormatted = action.replace(/[A-Z]/g, m => "-" + m.toLowerCase());
		this.state = actionFormatted;
		this.element.setAttribute("data-state", this.state);
	}
}
