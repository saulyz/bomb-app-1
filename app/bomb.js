import Explosives from "./explosives.js";

export default class Bomb extends Explosives {
	constructor(elementId, canvas, delay = 10) {
		super(elementId, canvas, ["doTrigger", "doDelay", "doExplode"]);
		this.delay = delay;

		this.init();
	}

	init() {
		this.render();
		this.element.addEventListener("click", this.executeAction.bind(this));
	}

	tick(callback, count = 1) {
		if (count) {
			setTimeout(() => {
				console.log("tick");
				this.tick(callback, --count);
			}, 100);
		} else {
			callback();
		}
	}

	doTrigger(callback) {
		console.log("starting - doTrigger");
		this.tick(callback);
	}

	doDelay(callback) {
		console.log("starting - doDelay");
		this.tick(callback, this.delay);
	}

	doExplode(callback) {
		console.log("starting - doExplode");
		this.tick(callback);
	}
}
